require 'rest-client'

class ExchangeController < ApplicationController
  layout 'application'

  def index
    if Rate.empty?
      json = external_api_call
      rate = Rate.new
      create_data(rate, json)
    end
    @rate = Rate.first
    @date, @time = date_time(@rate)
    @exchange_rate = calculate_exchange_rate(@rate.rates).round(2)

    respond_to do |format|
      format.html {}
      format.json {render(partial: 'exchange/index', locals: {rate: @rate, time: @time, date: @date, exchange_rate: @exchange_rate}, status: 200)}
    end

  end

  def refresh_data
    json = external_api_call
    rate = Rate.where(timestamp: json["timestamp"]).first
    if rate.blank?
      rate = Rate.first
      rate = update_data(rate, json)
    end
    date, time = date_time(rate) if rate.present?
    exchange_rate = calculate_exchange_rate(rate.rates).round(2)

    render(json: {rate: rate, time: time, date: date, exchange_rate: exchange_rate}, status: 200)

  end

  def sort_by_currency
    rate = Rate.first
    rates = rate.rates.sort_by{ |hash| hash[:curreny] }
    render(json: {rates: rates}, status: 200)
  end

  def sort_by_currency_value
    rate = Rate.first
    rates = rate.rates.sort_by{ |hash| hash[:value] }
    rates = rates.reverse
    render(json: {rates: rates}, status: 200)
  end


  private

  def update_data(rate, json)
    rate = record_data(rate, json)
    json["rates"].each do |currency, value|
      rate.rates.each do |hash|
        hash[:value] = value if hash[:currency] == currency
      end
    end
    rate.save!
    return rate
  end

  def create_data(rate, json)
    rate = record_data(rate, json)
    json["rates"].each do |currency, value|
      rate.rates << {currency: currency, value: value }
    end
    rate.save!
  end

  def record_data(rate, json)
    rate.disclaimer = json["disclaimer"]
    rate.license = json["license"]
    rate.timestamp = json["timestamp"]
    rate.base = json["base"]
    return rate
  end

  def external_api_call
    url = "https://openexchangerates.org/api/latest.json?app_id=65368373a86d467481480393f8180482&base=usd"
    page = ::RestClient.get(url)
    json = JSON.parse(page)
    return json
  end

  def date_time(rate)
    localtime = rate.updated_at.in_time_zone(TZInfo::Timezone.get('Asia/Kolkata'))
    time = localtime.strftime('%I:%M:%S %p')
    date = localtime.strftime('%d.%m.%Y')
    return date, time
  end

  def calculate_exchange_rate(rates)
    usd = 1.0
    cad = 1.0
    rates.each do |rate|
      if rate[:currency] == "CAD"
        cad = rate[:value]
        break
      end
    end
    return (usd/cad)
  end

end
