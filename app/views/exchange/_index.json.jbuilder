json.rates do
  json.date date
  json.time time
  json.exchange_rate exchange_rate
  json.disclaimer rate.disclaimer
  json.license rate.license
  json.base rate.base
  json.timestamp rate.timestamp

  json.partial! 'exchange/exchange_rates', rates: rate.rates

end
