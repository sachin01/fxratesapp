json.rates rates.each do |rate|
  json.currency rate['currency']
  json.value rate['value']
end
