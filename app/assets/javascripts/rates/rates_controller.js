angular.module('Rates', []);

angular.module('Rates').controller('RatesController', ['$scope', '$http', '$timeout',
function($scope, $http, $timeout) {

  // *********************************Private functions***********************************************

  // *********************************Public functions***********************************************
  $scope.sortByCurrency = function(){
    $http({
      method: "GET",
      url: "/currency_name",
      params: {}
    }).success(function(response){
      $scope.exchange_rates = response.rates;
    }).error(function(){
      $scope.error_message = "Something went wrong with the server";
    });
  };

  $scope.sortByCurrencyValue = function(){
    $http({
      method: "GET",
      url: "/currency_value",
      params: {}
    }).success(function(response){
      $scope.exchange_rates = response.rates;
    }).error(function(){
      $scope.error_message = "Something went wrong with the server";
    });
  };

  $scope.refreshData = function(){
    $http({
      method: "GET",
      url: "/refresh_data",
      params: {}
    }).success(function(response){
      $scope.exchange_rates = response.rate.rates;
      $scope.timestamp = response.rate.timestamp;
      $scope.date = response.date;
      $scope.time = response.time;
      $scope.exchange_rate_usd_cad = response.exchange_rate;
    }).error(function(){
      $scope.error_message = "Something went wrong with the server";
    });
  };


  // *********************************Initialization Code***********************************************

  var response = $('.js_rates_container').data('content');
  $scope.exchange_rates = response.rates.rates;
  $scope.date = response.rates.date;
  $scope.time = response.rates.time;
  $scope.disclaimer = response.rates.disclaimer;
  $scope.base = response.rates.base;
  $scope.timestamp = response.rates.timestamp;
  $scope.exchange_rate_usd_cad = response.rates.exchange_rate;
}]);
