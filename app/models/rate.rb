class Rate
  include Mongoid::Document
  include Mongoid::Timestamps

  store_in collection: "fx_rates"

  field :disclaimer, type: String
  field :license, type: String
  field :timestamp, type: Integer
  field :base, type: String
  field :rates, type: Array, default: []

  validates :timestamp, presence: true
  validates :base, presence: true

end
