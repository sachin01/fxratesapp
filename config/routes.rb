Rails.application.routes.draw do

  get '/' =>'exchange#index'
  get '/refresh_data' => 'exchange#refresh_data'
  get '/currency_name' => 'exchange#sort_by_currency'
  get '/currency_value' => 'exchange#sort_by_currency_value'
end
