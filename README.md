# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Git clone the app

* Make sure ruby v2.4.1 or above and rails v5.2.1 is installed

* Install latest mongodb database

* Run 'bundle install' command in project root folder.

* Access the app in a browser at localhost:3000
